import React from 'react'
import { FirebaseAppProvider } from 'reactfire'
import { BrowserRouter as Router } from 'react-router-dom'
import { firebaseConfig } from '../src/Firebase/firebase'
import { addDecorator } from '@storybook/react'

const withGlobal = (sb) => (
  <>
    <FirebaseAppProvider firebaseConfig={firebaseConfig}>
      <Router>{sb()}</Router>
    </FirebaseAppProvider>
  </>
)

addDecorator(withGlobal)

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
}
