export type Menu = {
  menuName: string
  foodstuffUsedList: { foodstuffId: string; amount: string }[]
}
