export type Foodstuff = {
  foodstuffName: string
  kcal: string
  carbohydrate: string
  protein: string
  lipid: string
  referenceAmount: string
  purchasingUnit: string
  salesOffice: string
  cost: string
  remarks: string
}

export type FoodstuffWithId = {
  id: string
  foodstuffName: string
  kcal: string
  carbohydrate: string
  protein: string
  lipid: string
  referenceAmount: string
  purchasingUnit: string
  salesOffice: string
  cost: string
  remarks: string
}
