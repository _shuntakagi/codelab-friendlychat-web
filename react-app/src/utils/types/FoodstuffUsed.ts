export type FoodstuffUsed = {
  foodstuffName: string
  foodstuffId: string
  amount: string
}
