import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import { FirebaseAppProvider } from 'reactfire'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import reportWebVitals from './reportWebVitals'
import { firebaseConfig } from './Firebase/firebase'
import { CheckAuth } from './CheckAuth'
import { Login } from './Components/Pages/Login'
import { My } from './Components/Pages/My'
import { Spinner } from './Components/Atoms/Spinner'
import { FoodstuffNew } from './Components/Pages/FoodstuffNew/Container'
import { FoodstuffEdit } from './Components/Pages/FoodstuffEdit/Container'
import { FoodstuffList } from './Components/Pages/FoodstuffList/Container'
import { MenuList } from './Components/Pages/MenuList/Container'
import { MenuNew } from './Components/Pages/MenuNew/Container'
import { MenuEdit } from './Components/Pages/MenuEdit/Container'

ReactDOM.render(
  <React.StrictMode>
    <FirebaseAppProvider firebaseConfig={firebaseConfig}>
      <Router>
        <Switch>
          <Suspense fallback={<Spinner />}>
            <Route exact path="/login" component={Login} />
            <CheckAuth>
              <Route exact path="/" component={My} />
              <Route exact path="/foodstuff/new" component={FoodstuffNew} />
              <Route
                exact
                path="/foodstuff/edit/:id"
                component={FoodstuffEdit}
              />
              <Route exact path="/foodstuff_list" component={FoodstuffList} />
              <Route exact path="/menu_list" component={MenuList} />
              <Route exact path="/menu/new" component={MenuNew} />
              <Route exact path="/menu/edit/:id" component={MenuEdit} />
            </CheckAuth>
          </Suspense>
        </Switch>
      </Router>
    </FirebaseAppProvider>
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
