import React from 'react'
import { useAuth, useUser } from 'reactfire'
import { Redirect } from 'react-router-dom'

export const CheckLoggedIn: React.FC = ({ children }) => {
  const { data: user } = useUser(useAuth(), { suspense: true })

  // if (user) {
  //   return <Redirect to="/" />
  // }

  return <>{children}</>
}
