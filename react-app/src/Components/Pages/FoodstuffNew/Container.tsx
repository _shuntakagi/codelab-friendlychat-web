import React, { useState } from 'react'
import { FoodstuffNewPresentation } from './Presentation'
import { useFirestore } from 'reactfire'
import { useHistory } from 'react-router-dom'

export const FoodstuffNew = () => {
  const db = useFirestore()
  const history = useHistory()

  const [foodstuffName, setFoodstuffName] = useState('')
  const [kcal, setKcal] = useState('')
  const [carbohydrate, setCarbohydrate] = useState('')
  const [protein, setProtein] = useState('')
  const [lipid, setLipid] = useState('')
  const [referenceAmount, setReferenceAmount] = useState('')
  const [purchasingUnit, setPurchasingUnit] = useState('')
  const [salesOffice, setSalesOffice] = useState('')
  const [cost, setCost] = useState('')
  const [remarks, setRemarks] = useState('')

  const formValue = {
    foodstuffName,
    kcal,
    carbohydrate,
    protein,
    lipid,
    referenceAmount,
    purchasingUnit,
    salesOffice,
    cost,
    remarks,
  }
  const register = async () => {
    await db
      .collection('foodstuffs')
      .add({ ...formValue })
      .then(() => history.push('/foodstuff_list'))
      .catch((e) => console.log('error', e))
  }

  const onClickBackButton = () => history.push('/foodstuff_list')

  return (
    <FoodstuffNewPresentation
      formValue={formValue}
      setFoodstuffName={(e) => setFoodstuffName(e.target.value)}
      setCarbohydrate={(e) => setCarbohydrate(e.target.value)}
      setCost={(e) => setCost(e.target.value)}
      setKcal={(e) => setKcal(e.target.value)}
      setLipid={(e) => setLipid(e.target.value)}
      setProtein={(e) => setProtein(e.target.value)}
      setPurchasingUnit={(e) => setPurchasingUnit(e.target.value)}
      setReferenceAmount={(e) => setReferenceAmount(e.target.value)}
      setRemarks={(e) => setRemarks(e.target.value)}
      setSalesOffice={(e) => setSalesOffice(e.target.value)}
      register={register}
      onClickBackButton={onClickBackButton}
    />
  )
}
