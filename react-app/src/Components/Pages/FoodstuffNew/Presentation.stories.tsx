import React from 'react'
import { FoodstuffNewPresentation } from './Presentation'
import { baseProps as foodstuffFormBaseProps } from '../../Organisms/FoodstuffForm/Presentation.stories'

export default { title: 'Pages/FoodstuffNew' }

const baseProps = { ...foodstuffFormBaseProps, onClickBackButton: () => {} }

export const basic = () => {
  return <FoodstuffNewPresentation {...baseProps} />
}
