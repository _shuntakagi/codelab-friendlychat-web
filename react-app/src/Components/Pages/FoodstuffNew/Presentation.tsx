import React from 'react'
import {
  FoodstuffForm,
  Props as FoodstuffFormProps,
} from '../../Organisms/FoodstuffForm/Presentation'
import { Header } from '../../Organisms/Header/Presentation'
import { Button } from '../../Atoms/Button/Presentation'

type Props = FoodstuffFormProps & { onClickBackButton: () => void }

export const FoodstuffNewPresentation: React.FC<Props> = ({
  formValue,
  register,
  setFoodstuffName,
  setCarbohydrate,
  setCost,
  setKcal,
  setLipid,
  setProtein,
  setPurchasingUnit,
  setReferenceAmount,
  setRemarks,
  setSalesOffice,
  onClickBackButton,
}) => {
  return (
    <>
      <Header />
      <FoodstuffForm
        formValue={formValue}
        setFoodstuffName={setFoodstuffName}
        setCarbohydrate={setCarbohydrate}
        setCost={setCost}
        setKcal={setKcal}
        setLipid={setLipid}
        setProtein={setProtein}
        setPurchasingUnit={setPurchasingUnit}
        setReferenceAmount={setReferenceAmount}
        setRemarks={setRemarks}
        setSalesOffice={setSalesOffice}
        register={register}
      />
      <Button onClick={onClickBackButton}>リストに戻る</Button>
    </>
  )
}
