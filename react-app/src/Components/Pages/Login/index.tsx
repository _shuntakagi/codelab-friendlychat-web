import React from 'react'
import { auth } from 'reactfire'
import { useHistory } from 'react-router-dom'
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth'
import styled from 'styled-components'

export const Login = () => {
  const history = useHistory()

  const uiConfig = {
    signInFlow: 'popup',
    signInOptions: [
      auth.GoogleAuthProvider.PROVIDER_ID,
      {
        provider: auth.EmailAuthProvider.PROVIDER_ID,
        requireDisplayName: true,
      },
    ],
    callbacks: {
      signInSuccessWithAuthResult: () => {
        history.push('/')
        return false
      },
    },
  }

  return (
    <Page>
      <Main>
        <MainInner>
          <Heading>ログイン</Heading>
          <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={auth()} />
        </MainInner>
      </Main>
    </Page>
  )
}

const Page = styled.div``

const Main = styled.main`
  width: 320px;
  margin-top: 105px;
  margin-left: auto;
  margin-right: auto;
  border-radius: 4px;
  background-color: #fff;
  box-shadow: 0 1px 6px 0 rgba(31, 69, 77, 0.16);
  text-align: center;
  border-left: 2px solid #fabb26;
  border-right: 2px solid #ed6029;

  &::after,
  &::before {
    content: '';
    display: block;
    height: 2px;
    background: linear-gradient(to right, #fabb26 0%, #ed6029 100%);
  }
`

const MainInner = styled.div`
  padding: 16px 0;
`

const Heading = styled.h1`
  font-size: 24px;
`
