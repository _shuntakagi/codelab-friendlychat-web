import React from 'react'
import { MenuEditPresentation } from './Presentation'
import { useHistory, useParams } from 'react-router-dom'
import { useFirestore, useFirestoreDocData } from 'reactfire'

export const MenuEdit = () => {
  const history = useHistory()
  const db = useFirestore()
  const { id }: { id: string } = useParams()

  const onClickBackButton = () => history.push('/menu_list')

  const { data } = useFirestoreDocData(db.collection('menu').doc(id), {
    suspense: true,
    idField: 'id',
  })

  return <MenuEditPresentation onClickBackButton={onClickBackButton} />
}
