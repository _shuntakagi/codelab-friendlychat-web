import React from 'react'
import {
  FoodstuffList,
  Props as FoodstuffListProps,
} from '../../Organisms/FoodstuffList/Presentation'
import { Header } from '../../Organisms/Header/Presentation'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import styled from 'styled-components'
import { Account } from '../../Molecules/Account/Container'

export type Props = FoodstuffListProps & {
  onClickAddButton: () => void
  onClickEditButton: (v: string) => void
}

export const FoodstuffListPagePresentation: React.FC<Props> = ({
  list,
  onClickAddButton,
  onClickEditButton,
}) => {
  if (!list || list.length === 0) {
    return <div />
  }

  return (
    <>
      <Account />
      <Header />
      {list && list.length > 0 && (
        <FoodstuffList list={list} onClickEditButton={onClickEditButton} />
      )}
      <Icon>
        <StyledAddCircleIcon
          fontSize="inherit"
          color="inherit"
          onClick={onClickAddButton}
        />
      </Icon>
    </>
  )
}

const StyledAddCircleIcon = styled(AddCircleIcon)`
  cursor: pointer;
  background-color: #fff;
`

const Icon = styled.div`
  font-size: 64px;
  color: #ff9100;
  position: fixed;
  bottom: 100px;
  right: 100px;
`
