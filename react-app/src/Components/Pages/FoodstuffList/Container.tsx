import React from 'react'
import { useFirestore, useFirestoreCollectionData } from 'reactfire'
import { FoodstuffListPagePresentation, Props } from './Presentation'
import { useHistory } from 'react-router-dom'

export const FoodstuffList = () => {
  const db = useFirestore()
  const history = useHistory()
  const { data } = useFirestoreCollectionData(db.collection('foodstuffs'), {
    suspense: true,
    idField: 'id',
  })

  const onClickAddButton = () => {
    history.push('/foodstuff/new')
  }

  const onClickEditButton = (id: string) => {
    history.push(`/foodstuff/edit/${id}`)
  }

  const list = (data as unknown) as Props['list']

  return (
    <>
      <FoodstuffListPagePresentation
        list={list}
        onClickAddButton={onClickAddButton}
        onClickEditButton={onClickEditButton}
      />
    </>
  )
}
