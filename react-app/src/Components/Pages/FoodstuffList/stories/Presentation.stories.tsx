import React from 'react'
import { FoodstuffListPagePresentation } from '../Presentation'
import { baseProps } from './data'

export default { title: 'Pages/FoodstuffList' }

export const basic = () => {
  return <FoodstuffListPagePresentation {...baseProps} />
}
