import { Props } from '../Presentation'
import { baseProps as foodstuffListBaseProps } from '../../../Organisms/FoodstuffList/stories/data'

export const baseProps: Props = {
  ...foodstuffListBaseProps,
  onClickAddButton: () => {},
  onClickEditButton: () => {},
}
