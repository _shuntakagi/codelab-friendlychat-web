import React, { useState } from 'react'
import { FoodstuffEditPresentation } from './Presentation'
import { useFirestore, useFirestoreDocData } from 'reactfire'
import { useHistory, useParams } from 'react-router-dom'
import { FormValues } from '../../Organisms/FoodstuffForm/Presentation'

export const FoodstuffEdit = () => {
  const db = useFirestore()
  const history = useHistory()
  const { id }: { id: string } = useParams()
  const query = db.collection('foodstuffs').doc(id)
  const { data } = useFirestoreDocData(query, {
    suspense: true,
  })
  const food = data as FormValues

  const [foodstuffName, setFoodstuffName] = useState(food.foodstuffName)
  const [kcal, setKcal] = useState(food.kcal)
  const [carbohydrate, setCarbohydrate] = useState(food.carbohydrate)
  const [protein, setProtein] = useState(food.protein)
  const [lipid, setLipid] = useState(food.lipid)
  const [referenceAmount, setReferenceAmount] = useState(food.referenceAmount)
  const [purchasingUnit, setPurchasingUnit] = useState(food.purchasingUnit)
  const [salesOffice, setSalesOffice] = useState(food.salesOffice)
  const [cost, setCost] = useState(food.cost)
  const [remarks, setRemarks] = useState(food.remarks)

  const formValue = {
    foodstuffName,
    kcal,
    carbohydrate,
    protein,
    lipid,
    referenceAmount,
    purchasingUnit,
    salesOffice,
    cost,
    remarks,
  }
  const register = async () => {
    await query
      .update({ ...formValue })
      .then(() => history.push('/foodstuff_list'))
      .catch((e) => console.log('error', e))
  }

  const onClickBackButton = () => history.push('/foodstuff_list')

  return (
    <FoodstuffEditPresentation
      formValue={formValue}
      setFoodstuffName={(e) => setFoodstuffName(e.target.value)}
      setCarbohydrate={(e) => setCarbohydrate(e.target.value)}
      setCost={(e) => setCost(e.target.value)}
      setKcal={(e) => setKcal(e.target.value)}
      setLipid={(e) => setLipid(e.target.value)}
      setProtein={(e) => setProtein(e.target.value)}
      setPurchasingUnit={(e) => setPurchasingUnit(e.target.value)}
      setReferenceAmount={(e) => setReferenceAmount(e.target.value)}
      setRemarks={(e) => setRemarks(e.target.value)}
      setSalesOffice={(e) => setSalesOffice(e.target.value)}
      register={register}
      onClickBackButton={onClickBackButton}
    />
  )
}
