import React from 'react'
import { FoodstuffEditPresentation } from './Presentation'
import { baseProps as foodstuffFormBaseProps } from '../../Organisms/FoodstuffForm/Presentation.stories'

export default { title: 'Pages/Foodstuff' }

const baseProps = { ...foodstuffFormBaseProps, onClickBackButton: () => {} }

export const basic = () => {
  return <FoodstuffEditPresentation {...baseProps} />
}
