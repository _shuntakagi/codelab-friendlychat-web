import React from 'react'
import { MenuForm } from '../../Organisms/MenuForm/Container'
import { Header } from '../../Organisms/Header/Presentation'
import { Button } from '../../Atoms/Button/Presentation'
import styled from 'styled-components'

type Props = { onClickBackButton: () => void }

export const MenuNewPresentation: React.FC<Props> = ({ onClickBackButton }) => {
  return (
    <>
      <Header />
      <MenuForm />
      <BackButton onClick={onClickBackButton}>リストに戻る</BackButton>
    </>
  )
}

const BackButton = styled(Button)`
  margin-top: 24px;
  width: 200px;
`
