import React from 'react'
import { MenuNewPresentation } from './Presentation'
import { useHistory } from 'react-router-dom'

export const MenuNew = () => {
  const history = useHistory()
  const onClickBackButton = () => history.push('/menu_list')

  return <MenuNewPresentation onClickBackButton={onClickBackButton} />
}
