import React from 'react'
import { MenuNewPresentation } from './Presentation'

export default { title: 'Pages/MenuNew' }

const baseProps = { onClickBackButton: () => {} }

export const basic = () => {
  return <MenuNewPresentation {...baseProps} />
}
