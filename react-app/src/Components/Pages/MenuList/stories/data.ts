import { Props } from '../Presentation'
import { baseProps as menuListBaseProps } from '../../../Organisms/MenuList/stories/data'

export const baseProps: Props = {
  ...menuListBaseProps,
  onClickAddButton: () => {},
  onClickEditButton: () => {},
}
