import React from 'react'
import { MenuListPagePresentation } from '../Presentation'
import { baseProps } from './data'

export default { title: 'Pages/MenuList' }

export const basic = () => {
  return <MenuListPagePresentation {...baseProps} />
}
