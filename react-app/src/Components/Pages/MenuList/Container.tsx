import React from 'react'
import { useFirestore, useFirestoreCollectionData } from 'reactfire'
import { MenuListPagePresentation, Props } from './Presentation'
import { useHistory } from 'react-router-dom'

export const MenuList = () => {
  const db = useFirestore()
  const history = useHistory()
  const { data } = useFirestoreCollectionData(db.collection('menu'), {
    suspense: true,
    idField: 'id',
  })

  const onClickAddButton = () => {
    history.push('/menu/new')
  }

  const onClickEditButton = (id: string) => {
    history.push(`/menu/edit/${id}`)
  }

  const list = (data as unknown) as Props['list']

  return (
    <>
      <MenuListPagePresentation
        list={list}
        onClickAddButton={onClickAddButton}
        onClickEditButton={onClickEditButton}
      />
    </>
  )
}
