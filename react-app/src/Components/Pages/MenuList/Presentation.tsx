import React from 'react'
import {
  MenuList,
  Props as MenuListProps,
} from '../../Organisms/MenuList/Presentation'
import { Header } from '../../Organisms/Header/Presentation'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import styled from 'styled-components'

export type Props = MenuListProps & {
  onClickAddButton: () => void
  onClickEditButton: (v: string) => void
}

export const MenuListPagePresentation: React.FC<Props> = ({
  list,
  onClickAddButton,
  onClickEditButton,
}) => {
  return (
    <>
      <Header />
      {list && list.length > 0 && (
        <MenuList list={list} onClickEditButton={onClickEditButton} />
      )}
      <Icon>
        <StyledAddCircleIcon
          fontSize="inherit"
          color="inherit"
          onClick={onClickAddButton}
        />
      </Icon>
    </>
  )
}

const StyledAddCircleIcon = styled(AddCircleIcon)`
  cursor: pointer;
`

const Icon = styled.div`
  font-size: 64px;
  color: #ff9100;
  position: fixed;
  bottom: 100px;
  right: 100px;
`
