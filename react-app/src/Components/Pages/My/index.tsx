import React from 'react'
import styled from 'styled-components'
import { useFirestore, useFirestoreCollectionData } from 'reactfire'
import { Account } from '../../Molecules/Account/Container'
import { Header } from '../../Organisms/Header/Presentation'
import { Link } from 'react-router-dom'

type Messages = {
  id: string
  name: string
  profilePicUrl: string
  text: string
  timestamp: string
}

export const My: React.FC = () => {
  const db = useFirestore()

  const { data } = useFirestoreCollectionData(
    db.collection('messages').orderBy('timestamp'),
    { idField: 'id', suspense: true }
  )

  return (
    <Container>
      <Account />
      <Header />
      <CardList className="App">
        {data.length > 0 &&
          ((data as unknown) as Messages[]).map((message) => (
            <Card key={message.id}>
              <Li>{message.id}</Li>
              <Li>{message.name}</Li>
              <Li>{message.text}</Li>
            </Card>
          ))}
      </CardList>
      <Link to="/foodstuff">foodstuff</Link>
    </Container>
  )
}

const Container = styled.div`
  width: 1200px;
`

const CardList = styled.ul`
  display: flex;
  flex-wrap: wrap;
`

const Card = styled.ul`
  width: 200px;
  padding: 20px;
`

const Li = styled.li`
  list-style: none;
  word-break: break-all;
`
