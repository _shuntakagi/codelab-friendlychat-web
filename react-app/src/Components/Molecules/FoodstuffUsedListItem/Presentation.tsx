import React, { useCallback } from 'react'
import { Input } from '../../Atoms/Input/Presentation'
import styled from 'styled-components'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { FoodstuffWithId } from '../../../utils/types/Foodstuff'
import { FoodstuffUsed } from '../../../utils/types/FoodstuffUsed'
import { useFirestoreDocData } from 'reactfire'

export type Props = {
  allFoodstuffs: FoodstuffWithId[]
  targetKey: number
  item: FoodstuffUsed
  onClickDeleteFoodstuff: (index: number) => void
  onChangeFoodstuffName: (
    index: number,
    target: string,
    foodstuffId: string
  ) => void
  onChangePotion: (index: number, target: string) => void
}

export const FoodstuffUsedListItem: React.FC<Props> = ({
  allFoodstuffs,
  item,
  targetKey,
  onClickDeleteFoodstuff,
  onChangeFoodstuffName,
  onChangePotion,
}) => {
  const memoOnChangeFoodstuffName = useCallback(
    (e: React.ChangeEvent<{}>, v: FoodstuffWithId | null, _reason: string) => {
      onChangeFoodstuffName(targetKey, v ? v.foodstuffName : '', v ? v.id : '')
    },
    [onChangeFoodstuffName, targetKey]
  )
  const memoOnChangePotion = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      onChangePotion(targetKey, e.target.value)
    },
    [onChangePotion, targetKey]
  )
  const memoOnClickDeleteFoodstuff = useCallback(() => {
    onClickDeleteFoodstuff(targetKey)
  }, [onClickDeleteFoodstuff, targetKey])

  return (
    <ItemContainer>
      <Autocomplete
        id="combo-box-demo"
        options={allFoodstuffs}
        getOptionLabel={(option) => option.foodstuffName}
        onChange={memoOnChangeFoodstuffName}
        // inputValue={item.foodstuffName}
        renderInput={(params) => (
          <div ref={params.InputProps.ref}>
            <Input
              {...params.inputProps}
              // value={item.foodstuffName}
              placeholder="食材名"
            />
          </div>
        )}
      />
      <Input
        value={item.amount}
        onChange={memoOnChangePotion}
        placeholder="分量"
      />
      g
      <DeleteForeverIcon onClick={memoOnClickDeleteFoodstuff} />
    </ItemContainer>
  )
}

const ItemContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  &:not(:first-child) {
    margin-top: 16px;
  }
`
