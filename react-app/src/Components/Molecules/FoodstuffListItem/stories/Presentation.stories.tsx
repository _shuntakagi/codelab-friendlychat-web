import React from 'react'
import { FoodstuffListItem } from '../Presentation'
import { baseProps } from './data'

export default { title: 'Molecules/FoodstuffListItem' }

export const basic = () => {
  return <FoodstuffListItem {...baseProps} />
}
