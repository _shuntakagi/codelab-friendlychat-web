import React from 'react'
import styled from 'styled-components'
import { Button } from '../../Atoms/Button/Presentation'

export type Props = {
  foodstuffName: string
  cost: string
  kcal: string
  carbohydrate: string
  protein: string
  lipid: string
  onClickEditButton: () => void
}

export const FoodstuffListItem: React.FC<Props> = ({
  foodstuffName,
  cost,
  kcal,
  carbohydrate,
  protein,
  lipid,
  onClickEditButton,
}) => {
  return (
    <Container>
      <ImgArea>
        <Button onClick={onClickEditButton}>編集</Button>
      </ImgArea>
      <Table>
        <tbody>
          <Tr>
            <Td>名称</Td>
            <Td>{foodstuffName}</Td>
          </Tr>
          <Tr>
            <Td>費用</Td>
            <Td>{cost} 円</Td>
          </Tr>
          <Tr>
            <Td>カロリー</Td>
            <Td>{kcal} kcal</Td>
          </Tr>
          <Tr>
            <Td>炭水化物</Td>
            <Td>{carbohydrate} g</Td>
          </Tr>
          <Tr>
            <Td>タンパク質</Td>
            <Td>{protein} g</Td>
          </Tr>
          <Tr>
            <Td>脂質</Td>
            <Td>{lipid} g</Td>
          </Tr>
        </tbody>
      </Table>
    </Container>
  )
}

const Container = styled.div`
  width: 200px;
  margin: 0 32px 32px 0;
`

const ImgArea = styled.div`
  width: 100%;
  height: 120px;
  background-color: #bbbbbb;
`

const Table = styled.table`
  margin-top: 16px;
  border-collapse: collapse;
  width: 100%;
`

const Tr = styled.tr`
  border: 1px solid;
`

const Td = styled.td`
  border: 1px solid;
  padding: 8px;
  line-height: 1;
`
