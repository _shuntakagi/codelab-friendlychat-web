import React, { useState } from 'react'
import { Presentation } from './Presentation'
import { useHistory } from 'react-router-dom'
import { useAuth, useUser } from 'reactfire'

export const Account = () => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false)
  const history = useHistory()
  const auth = useAuth()
  const { data: user } = useUser()

  const modalOpen = () => setIsModalOpen(true)
  const modalClose = () => setIsModalOpen(false)

  const logout = async () => {
    await auth.signOut()
    history.push('/login')
  }
  return (
    <Presentation
      logout={logout}
      modalOpen={modalOpen}
      modalClose={modalClose}
      isModalOpen={isModalOpen}
      photoUrl={user?.photoURL ?? undefined}
    />
  )
}
