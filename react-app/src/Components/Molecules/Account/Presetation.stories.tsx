import React from 'react'
import { noop } from 'remeda'
import { Presentation } from './Presentation'

export default {
  component: Presentation,
  title: 'Molecules/Account',
}

export const Default = () => (
  <Presentation
    logout={noop}
    modalOpen={noop}
    modalClose={noop}
    isModalOpen={false}
  />
)
export const ModalOpen = () => (
  <Presentation
    logout={noop}
    modalOpen={noop}
    modalClose={noop}
    isModalOpen={true}
  />
)
