import React from 'react'
import styled from 'styled-components'
import { Avatar } from '../../Atoms/Avatar/Presentation'

type Props = {
  logout: () => void
  modalOpen: () => void
  modalClose: () => void
  isModalOpen: boolean
  photoUrl?: string
}

export const Presentation: React.FC<Props> = ({
  logout,
  modalOpen,
  modalClose,
  isModalOpen,
  photoUrl,
}) => {
  return (
    <>
      <div onClick={modalOpen}>
        <Avatar photoUrl={photoUrl} />
      </div>
      {isModalOpen && (
        <>
          <Mask onClick={modalClose} />
          <Modal>
            <button type="button" onClick={logout}>
              ログアウト
            </button>
          </Modal>
        </>
      )}
    </>
  )
}

const Modal = styled.div`
  width: 200px;
  padding: 40px;
  background: #fff;
  border: 1px solid #ccc;
  border-color: rgba(0, 0, 0, 0.2);
  border-radius: 8px;
  box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
  position: absolute;
  left: 8px;
  top: 62px;
  z-index: 2;
`

const Mask = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  z-index: 1;
`
