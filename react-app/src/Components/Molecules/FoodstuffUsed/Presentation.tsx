import React from 'react'
import styled from 'styled-components'
import { FoodstuffUsedListItem } from '../FoodstuffUsedListItem/Presentation'
import { FoodstuffWithId } from '../../../utils/types/Foodstuff'
import { FoodstuffUsed } from '../../../utils/types/FoodstuffUsed'

export type Props = {
  allFoodstuffs: FoodstuffWithId[]
  foodstuffUsedList: FoodstuffUsed[]
  onClickDeleteFoodstuff: (index: number) => void
  onChangeFoodstuffName: (
    index: number,
    target: string,
    foodstuffId: string
  ) => void
  onChangePotion: (index: number, target: string) => void
}

export const FoodstuffUsedList: React.FC<Props> = ({
  allFoodstuffs,
  foodstuffUsedList,
  onClickDeleteFoodstuff,
  onChangeFoodstuffName,
  onChangePotion,
}) => {
  return (
    <Container>
      {foodstuffUsedList.map((item, index) => (
        <FoodstuffUsedListItem
          key={index}
          allFoodstuffs={allFoodstuffs}
          item={item}
          targetKey={index}
          onClickDeleteFoodstuff={onClickDeleteFoodstuff}
          onChangeFoodstuffName={onChangeFoodstuffName}
          onChangePotion={onChangePotion}
        />
      ))}
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 400px;
`
