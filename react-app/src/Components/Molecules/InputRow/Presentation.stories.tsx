import React from 'react'
import { InputRow } from './Presentation'

export default { title: 'Molecules/InputRow' }

export const Basic = () => {
  return (
    <InputRow value="" labelText="カロリー" id="kcal" onChange={() => {}} />
  )
}
