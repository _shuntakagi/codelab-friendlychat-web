import React from 'react'
import { Input } from '../../Atoms/Input/Presentation'
import styled from 'styled-components'

type Props = {
  id: string
  labelText: string
  value: string
  onChange: (v: React.ChangeEvent<HTMLInputElement>) => void
  placeholder?: string
}

export const InputRow: React.FC<Props> = ({
  id,
  labelText,
  value,
  onChange,
  placeholder,
}) => {
  return (
    <InputContainer>
      <label htmlFor={id}>{labelText}</label>
      <StyledInput
        id={id}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
      />
    </InputContainer>
  )
}

const InputContainer = styled.div`
  display: flex;
  align-items: center;
`

const StyledInput = styled(Input)`
  margin-left: 8px;
`
