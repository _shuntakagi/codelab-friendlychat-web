import { Props } from '../Presentation'

export const baseProps: Props = {
  menuName: 'カレー',
  cost: '100',
  kcal: '110',
  carbohydrate: '10',
  protein: '20',
  lipid: '30',
  onClickEditButton: () => {},
}
