import React from 'react'
import { MenuListItem } from '../Presentation'
import { baseProps } from './data'

export default { title: 'Molecules/MenuListItem' }

export const basic = () => {
  return <MenuListItem {...baseProps} />
}
