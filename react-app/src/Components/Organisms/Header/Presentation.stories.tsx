import React from 'react'
import { Header } from './Presentation'

export default { title: 'Organisms/Header' }

export const basic = () => {
  return <Header />
}
