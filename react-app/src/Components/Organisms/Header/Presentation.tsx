import React from 'react'
import { Link } from 'react-router-dom'

export const Header = () => {
  return (
    <header>
      <ul>
        <li>
          <Link to={'/foodstuff_list'}>食材リスト</Link>
        </li>
        <li>
          <Link to={'/menu_list'}>メニューリスト</Link>
        </li>
      </ul>
    </header>
  )
}
