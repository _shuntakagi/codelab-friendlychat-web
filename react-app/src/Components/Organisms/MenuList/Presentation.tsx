import React from 'react'
import styled from 'styled-components'
import {
  MenuListItem,
  Props as MenuItemProps,
} from '../../Molecules/MenuListItem/Presentation'

export type Props = {
  list: Array<MenuItemProps & { id: string }>
  onClickEditButton: (v: string) => void
}

export const MenuList: React.FC<Props> = ({ list, onClickEditButton }) => {
  return (
    <Container>
      {list.map((item) => (
        <MenuListItem
          key={item.id}
          menuName={item.menuName}
          cost={item.cost}
          kcal={item.kcal}
          carbohydrate={item.carbohydrate}
          protein={item.protein}
          lipid={item.lipid}
          onClickEditButton={() => onClickEditButton(item.id)}
        />
      ))}
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
`
