import { baseProps as MenuListItemBaseProps } from '../../../Molecules/MenuListItem/stories/data'
import { Props } from '../Presentation'

export const baseProps: Props = {
  list: [
    {
      id: '1',
      ...MenuListItemBaseProps,
    },
    {
      id: '2',
      ...MenuListItemBaseProps,
    },
    {
      id: '3',
      ...MenuListItemBaseProps,
    },
    {
      id: '4',
      ...MenuListItemBaseProps,
    },
    {
      id: '5',
      ...MenuListItemBaseProps,
    },
    {
      id: '6',
      ...MenuListItemBaseProps,
    },
    {
      id: '7',
      ...MenuListItemBaseProps,
    },
  ],
  onClickEditButton: () => {},
}
