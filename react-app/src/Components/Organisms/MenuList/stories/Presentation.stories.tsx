import React from 'react'
import { MenuList } from '../Presentation'
import { baseProps } from './data'

export default { title: 'Organisms/MenuList' }

export const basic = () => {
  return <MenuList {...baseProps} />
}
