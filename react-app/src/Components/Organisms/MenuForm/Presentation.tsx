import React from 'react'
import { InputRow } from '../../Molecules/InputRow/Presentation'
import styled from 'styled-components'
import { Button } from '../../Atoms/Button/Presentation'
import { FoodstuffUsedList } from '../../Molecules/FoodstuffUsed/Presentation'
import { FoodstuffWithId } from '../../../utils/types/Foodstuff'
import { FoodstuffUsed } from '../../../utils/types/FoodstuffUsed'

export type Props = {
  foodstuffs: FoodstuffWithId[]
  foodstuffUsedList: FoodstuffUsed[]
  menuName: string
  onChangeFoodstuffName: (
    index: number,
    value: string,
    foodstuffId: string
  ) => void
  onChangePotion: (index: number, value: string) => void
  onClickAddFoodstuff: () => void
  onClickDeleteFoodstuff: (index: number) => void
  onChangeMenuName: (e: React.ChangeEvent<HTMLInputElement>) => void
  onClickRegister: () => void
}

export const MenuFormPresentation: React.FC<Props> = ({
  foodstuffs,
  foodstuffUsedList,
  menuName,
  onChangeFoodstuffName,
  onChangePotion,
  onClickAddFoodstuff,
  onClickDeleteFoodstuff,
  onChangeMenuName,
  onClickRegister,
}) => {
  return (
    <>
      <h1>メニュー登録</h1>
      <FormContainer id="menuForm" onSubmit={(event) => event.preventDefault()}>
        <InputRow
          id="menuName"
          labelText="名称"
          value={menuName}
          onChange={onChangeMenuName}
        />
        <h2>使用食材</h2>
        <AddFoodstuffUsedButton type="button" onClick={onClickAddFoodstuff}>
          食材追加
        </AddFoodstuffUsedButton>
        <FoodstuffUsedListContainer>
          <FoodstuffUsedList
            allFoodstuffs={foodstuffs}
            foodstuffUsedList={foodstuffUsedList}
            onClickDeleteFoodstuff={onClickDeleteFoodstuff}
            onChangeFoodstuffName={onChangeFoodstuffName}
            onChangePotion={onChangePotion}
          />
        </FoodstuffUsedListContainer>
        <RegisterButton form="menuForm" type="submit" onClick={onClickRegister}>
          登録
        </RegisterButton>
      </FormContainer>
    </>
  )
}

const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
`

const AddFoodstuffUsedButton = styled(Button)`
  width: 200px;
`

const FoodstuffUsedListContainer = styled.div`
  margin-top: 16px;
`

const RegisterButton = styled(Button)`
  margin-top: 16px;
  width: 200px;
`
