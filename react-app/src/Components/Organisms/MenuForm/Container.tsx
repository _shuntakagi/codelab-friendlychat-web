import React, { useState } from 'react'
import { MenuFormPresentation } from './Presentation'
import { useFirestore, useFirestoreCollectionData } from 'reactfire'
import { FoodstuffWithId } from '../../../utils/types/Foodstuff'
import { FoodstuffUsed } from '../../../utils/types/FoodstuffUsed'
import { useHistory } from 'react-router-dom'

type Props = {}

export const MenuForm: React.FC<Props> = () => {
  const db = useFirestore()
  const history = useHistory()

  const [foodstuffsUsed, setFoodstuffs] = useState<FoodstuffUsed[]>([])
  const [menuName, setMenuName] = useState<string>('')

  const onChangeMenuName = (e: React.ChangeEvent<HTMLInputElement>) =>
    setMenuName(e.target.value)

  const onChangeFoodstuffName = (
    index: number,
    value: string,
    foodstuffId: string
  ) => {
    const list = Object.assign([] as typeof foodstuffsUsed[], foodstuffsUsed)
    list[index]['foodstuffName'] = value
    list[index]['foodstuffId'] = foodstuffId
    setFoodstuffs([...list])
  }

  const onChangePotion = (index: number, value: string) => {
    const list = Object.assign([] as typeof foodstuffsUsed[], foodstuffsUsed)
    list[index]['amount'] = value
    setFoodstuffs([...list])
  }

  const onClickAddFoodstuff = () => {
    setFoodstuffs([
      ...foodstuffsUsed,
      { foodstuffName: '', amount: '', foodstuffId: '' },
    ])
  }

  const onClickDeleteFoodstuff = (index: number) => {
    const tmp = Object.assign([], foodstuffsUsed)
    tmp.splice(index, 1)
    setFoodstuffs([...tmp])
  }

  const onClickRegister = async () => {
    await db
      .collection('menu')
      .add({
        menuName,
        foodstuffUsedList: foodstuffsUsed.map((item) => ({
          foodstuff: db.collection('foodstuffs').doc(item.foodstuffId),
          amount: item.amount,
        })),
      })
      .then(() => history.push('/menu_list'))
      .catch((e) => console.log('error', e))
  }

  const { data } = useFirestoreCollectionData(db.collection('foodstuffs'), {
    suspense: true,
    idField: 'id',
  })

  const allFoodstuffs = (data as unknown) as FoodstuffWithId[]

  return (
    <MenuFormPresentation
      foodstuffUsedList={foodstuffsUsed}
      foodstuffs={allFoodstuffs}
      onChangeMenuName={onChangeMenuName}
      menuName={menuName}
      onChangeFoodstuffName={onChangeFoodstuffName}
      onChangePotion={onChangePotion}
      onClickAddFoodstuff={onClickAddFoodstuff}
      onClickDeleteFoodstuff={onClickDeleteFoodstuff}
      onClickRegister={onClickRegister}
    />
  )
}
