import React from 'react'
import { FoodstuffForm, Props, FormValues } from './Presentation'

export default { title: 'Organisms/FoodstuffForm' }

export const empty = () => {
  return <FoodstuffForm {...baseProps} />
}

export const filled = () => {
  return <FoodstuffForm {...filledProps} />
}

export const baseFormValue = {
  foodstuffName: '',
  kcal: '',
  carbohydrate: '',
  protein: '',
  lipid: '',
  referenceAmount: '',
  purchasingUnit: '',
  salesOffice: '',
  cost: '',
  remarks: '',
}

export const baseProps: Props = {
  formValue: baseFormValue,
  register: () => {},
  setFoodstuffName: () => {},
  setKcal: () => {},
  setCarbohydrate: () => {},
  setProtein: () => {},
  setLipid: () => {},
  setReferenceAmount: () => {},
  setPurchasingUnit: () => {},
  setSalesOffice: () => {},
  setCost: () => {},
  setRemarks: () => {},
}

const filledFormValue: FormValues = {
  foodstuffName: '人参',
  kcal: '10',
  carbohydrate: '10',
  protein: '10',
  lipid: '10',
  referenceAmount: '10',
  purchasingUnit: '100',
  salesOffice: 'イトーヨーカドー',
  cost: '200',
  remarks: '購買単位 1本 100g',
}

const filledProps: Props = {
  ...baseProps,
  formValue: {
    ...filledFormValue,
  },
}
