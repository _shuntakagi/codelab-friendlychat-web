import React from 'react'
import { InputRow } from '../../Molecules/InputRow/Presentation'
import styled from 'styled-components'
import { Button } from '../../Atoms/Button/Presentation'

export type FormValues = {
  foodstuffName: string
  kcal: string
  carbohydrate: string
  protein: string
  lipid: string
  referenceAmount: string
  purchasingUnit: string
  salesOffice: string
  cost: string
  remarks: string
}
export type Props = {
  formValue: FormValues
  register: () => void
  setFoodstuffName: (v: React.ChangeEvent<HTMLInputElement>) => void
  setKcal: (v: React.ChangeEvent<HTMLInputElement>) => void
  setCarbohydrate: (v: React.ChangeEvent<HTMLInputElement>) => void
  setProtein: (v: React.ChangeEvent<HTMLInputElement>) => void
  setLipid: (v: React.ChangeEvent<HTMLInputElement>) => void
  setReferenceAmount: (v: React.ChangeEvent<HTMLInputElement>) => void
  setPurchasingUnit: (v: React.ChangeEvent<HTMLInputElement>) => void
  setSalesOffice: (v: React.ChangeEvent<HTMLInputElement>) => void
  setCost: (v: React.ChangeEvent<HTMLInputElement>) => void
  setRemarks: (v: React.ChangeEvent<HTMLInputElement>) => void
}

export const FoodstuffForm: React.FC<Props> = ({
  formValue,
  register,
  setFoodstuffName,
  setCarbohydrate,
  setCost,
  setKcal,
  setLipid,
  setProtein,
  setPurchasingUnit,
  setReferenceAmount,
  setRemarks,
  setSalesOffice,
}) => {
  return (
    <>
      <h1>食材新規登録</h1>
      <form id="foodstuffForm" onSubmit={(event) => event.preventDefault()}>
        <FormContainer>
          <InputRow
            id="foodstuffName"
            labelText="名称"
            value={formValue.foodstuffName}
            onChange={setFoodstuffName}
          />
          <InputRow
            id="kcal"
            labelText="カロリー"
            value={formValue.kcal}
            onChange={setKcal}
          />
          <InputRow
            id="carbohydrate"
            labelText="炭水化物"
            value={formValue.carbohydrate}
            onChange={setCarbohydrate}
          />
          <InputRow
            id="protein"
            labelText="タンパク質"
            value={formValue.protein}
            onChange={setProtein}
          />
          <InputRow
            id="lipid"
            labelText="脂質"
            value={formValue.lipid}
            onChange={setLipid}
          />
          <InputRow
            id="referenceAmount"
            labelText="基準量"
            value={formValue.referenceAmount}
            onChange={setReferenceAmount}
          />
          <InputRow
            id="purchasingUnit"
            labelText="購買単位"
            value={formValue.purchasingUnit}
            onChange={setPurchasingUnit}
          />
          <InputRow
            id="salesOffice"
            labelText="販売所"
            value={formValue.salesOffice}
            onChange={setSalesOffice}
          />
          <InputRow
            id="cost"
            labelText="費用"
            value={formValue.cost}
            onChange={setCost}
          />
          <InputRow
            id="remarks"
            labelText="備考"
            value={formValue.remarks}
            onChange={setRemarks}
          />
        </FormContainer>
        <Button onClick={register}>登録</Button>
      </form>
    </>
  )
}

const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
`
