import React from 'react'
import { FoodstuffList } from '../Presentation'
import { baseProps } from './data'

export default { title: 'Organisms/FoodstuffList' }

export const basic = () => {
  return <FoodstuffList {...baseProps} />
}
