import { baseProps as FoodstuffListItemBaseProps } from '../../../Molecules/FoodstuffListItem/stories/data'
import { Props } from '../Presentation'

export const baseProps: Props = {
  list: [
    {
      id: '1',
      ...FoodstuffListItemBaseProps,
    },
    {
      id: '2',
      ...FoodstuffListItemBaseProps,
    },
    {
      id: '3',
      ...FoodstuffListItemBaseProps,
    },
    {
      id: '4',
      ...FoodstuffListItemBaseProps,
    },
    {
      id: '5',
      ...FoodstuffListItemBaseProps,
    },
    {
      id: '6',
      ...FoodstuffListItemBaseProps,
    },
    {
      id: '7',
      ...FoodstuffListItemBaseProps,
    },
  ],
  onClickEditButton: () => {},
}
