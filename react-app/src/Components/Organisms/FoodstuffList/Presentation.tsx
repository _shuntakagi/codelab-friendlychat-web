import React from 'react'
import styled from 'styled-components'
import {
  FoodstuffListItem,
  Props as FoodstuffListItemProps,
} from '../../Molecules/FoodstuffListItem/Presentation'

export type Props = {
  list: Array<FoodstuffListItemProps & { id: string }>
  onClickEditButton: (v: string) => void
}

export const FoodstuffList: React.FC<Props> = ({ list, onClickEditButton }) => {
  return (
    <Container>
      {list.map((item) => (
        <FoodstuffListItem
          key={item.id}
          foodstuffName={item.foodstuffName}
          cost={item.cost}
          kcal={item.kcal}
          carbohydrate={item.carbohydrate}
          protein={item.protein}
          lipid={item.lipid}
          onClickEditButton={() => onClickEditButton(item.id)}
        />
      ))}
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
`
