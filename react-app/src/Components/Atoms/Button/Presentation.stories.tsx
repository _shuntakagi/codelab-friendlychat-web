import React from 'react'
import { Button } from './Presentation'

export default { title: 'Atoms/Button' }

export const Basic = () => {
  return <Button>ボタン</Button>
}
