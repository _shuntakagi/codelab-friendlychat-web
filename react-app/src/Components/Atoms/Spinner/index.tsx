import React from 'react'
import styled, { keyframes } from 'styled-components'

export const Spinner: React.FC = () => {
  return (
    <Container>
      <Dot1 />
      <Dot2 />
      <Dot />
    </Container>
  )
}

const Container = styled.div`
  margin: 100px auto 0;
  width: 70px;
  text-align: center;
`

const BounceDelay = keyframes`
  0%, 80%, 100% {
    transform: scale(0);
  } 40% {
      transform: scale(1.0);
    }
`

const Dot = styled.div`
  width: 18px;
  height: 18px;
  border-radius: 100%;
  background-color: #ed6029;
  display: inline-block;
  animation: ${BounceDelay} 1.4s infinite ease-in-out both;
`

const Dot1 = styled(Dot)`
  animation-delay: -0.32s;
  background-color: #fabb26;
`

const Dot2 = styled(Dot)`
  animation-delay: -0.16s;
  background-color: #f58d28;
`
