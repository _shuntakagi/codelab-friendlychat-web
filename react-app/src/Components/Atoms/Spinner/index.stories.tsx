import React from 'react'
import { Spinner } from './'

export default {
  component: Spinner,
  title: 'Atoms/Spinner',
}

export const Default = () => <Spinner />
