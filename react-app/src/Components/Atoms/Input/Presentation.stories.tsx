import React from 'react'
import { Input, Select } from './Presentation'

export default { title: 'Atoms/Input' }

export const TextInput = () => {
  return <Input />
}

export const SelectInput = () => {
  return <Select />
}
