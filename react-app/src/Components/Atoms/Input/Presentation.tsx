import styled from 'styled-components'

export const Input = styled.input`
  height: 30px;
  padding: 0 8px;
  line-height: 28px;
`

export const Select = styled.select``
