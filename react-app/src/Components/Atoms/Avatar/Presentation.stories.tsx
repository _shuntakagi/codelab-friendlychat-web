import React from 'react'
import { Avatar } from './Presentation'

export default {
  component: Avatar,
  title: 'Atoms/Avatar',
}

export const Default = () => <Avatar />
