import React from 'react'
import styled from 'styled-components'

export const Avatar = ({ photoUrl }: { photoUrl?: string }) => {
  return (
    <Img
      src={photoUrl ?? `${process.env.PUBLIC_URL}/avatar.png`}
      alt="Avatar"
    />
  )
}

const Img = styled.img`
  vertical-align: middle;
  width: 50px;
  height: 50px;
  border-radius: 50%;
`
